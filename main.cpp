#include <Wire.h>
#include <TimeLib.h>
#include <DS1307RTC.h>
#include <LiquidCrystal_I2C.h>

int alarmHour = 7, alarmMinute = 0;

LiquidCrystal_I2C lcd(0x27, 16, 2);
tmElements_t clock;

void alarm()
{
	if (clock.Hour == alarmHour && clock.Minute == alarmMinute)
		tone(10, 5000);

	if (digitalRead(12) == LOW)
		noTone(10);
}

void setAlarm()
{
	lcd.backlight();
	while (digitalRead(12) != LOW)
	{

		lcd.setCursor(0, 1);
		lcd.print("BUDZIK:");
		lcd.print(alarmHour);
		lcd.print(":");
		lcd.print(alarmMinute);

		if (digitalRead(13) == LOW)
		{
			alarmHour++;
		}
		if (digitalRead(11) == LOW)
		{
			alarmHour--;
		}
		if (alarmHour > 23)
			alarmHour = 0;
		if (alarmHour < 0)
			alarmHour = 23;
	}

	while (digitalRead(12) != LOW)
	{
		lcd.setCursor(0, 1);
		lcd.print("BUDZIK:");
		lcd.print(alarmHour);
		lcd.print(":");
		lcd.print(alarmMinute);
		if (digitalRead(13) == LOW)
		{
			alarmMinute++;
		}
		if (digitalRead(11) == LOW)
		{
			alarmMinute--;
		}
		if (alarmHour > 59)
			alarmHour = 0;
		if (alarmMinute < 0)
			alarmHour = 59;
	}
	lcd.noBacklight();
	delay(100);
}

void SecondSet()
{
	switch (clock.Second)
	{
	case 0:
		digitalWrite(30, HIGH);
		digitalWrite(38, LOW);
		break;

	case 1:
		digitalWrite(30, HIGH);
		digitalWrite(39, LOW);
		break;

	case 2:
		digitalWrite(30, HIGH);
		digitalWrite(40, LOW);
		break;

	case 3:
		digitalWrite(30, HIGH);
		digitalWrite(41, LOW);
		break;

	case 4:
		digitalWrite(30, HIGH);
		digitalWrite(42, LOW);
		break;

	case 5:
		digitalWrite(30, HIGH);
		digitalWrite(43, LOW);
		break;

	case 6:
		digitalWrite(30, HIGH);
		digitalWrite(44, LOW);
		break;

	case 7:
		digitalWrite(30, HIGH);
		digitalWrite(45, LOW);
		break;

	case 8:
		digitalWrite(31, HIGH);
		digitalWrite(38, LOW);
		break;

	case 9:
		digitalWrite(31, HIGH);
		digitalWrite(39, LOW);
		break;

	case 10:
		digitalWrite(31, HIGH);
		digitalWrite(40, LOW);
		break;

	case 11:
		digitalWrite(31, HIGH);
		digitalWrite(41, LOW);
		break;

	case 12:
		digitalWrite(31, HIGH);
		digitalWrite(42, LOW);
		break;

	case 13:
		digitalWrite(31, HIGH);
		digitalWrite(43, LOW);
		break;

	case 14:
		digitalWrite(31, HIGH);
		digitalWrite(44, LOW);
		break;

	case 15:
		digitalWrite(31, HIGH);
		digitalWrite(45, LOW);
		break;

	case 16:
		digitalWrite(32, HIGH);
		digitalWrite(38, LOW);
		break;

	case 17:
		digitalWrite(32, HIGH);
		digitalWrite(39, LOW);
		break;

	case 18:
		digitalWrite(32, HIGH);
		digitalWrite(40, LOW);
		break;

	case 19:
		digitalWrite(32, HIGH);
		digitalWrite(41, LOW);
		break;

	case 20:
		digitalWrite(32, HIGH);
		digitalWrite(42, LOW);
		break;

	case 21:
		digitalWrite(32, HIGH);
		digitalWrite(43, LOW);
		break;

	case 22:
		digitalWrite(32, HIGH);
		digitalWrite(44, LOW);
		break;

	case 23:
		digitalWrite(32, HIGH);
		digitalWrite(45, LOW);
		break;

	case 24:
		digitalWrite(33, HIGH);
		digitalWrite(38, LOW);
		break;

	case 25:
		digitalWrite(33, HIGH);
		digitalWrite(39, LOW);
		break;

	case 26:
		digitalWrite(33, HIGH);
		digitalWrite(40, LOW);
		break;

	case 27:
		digitalWrite(33, HIGH);
		digitalWrite(41, LOW);
		break;

	case 28:
		digitalWrite(33, HIGH);
		digitalWrite(42, LOW);
		break;

	case 29:
		digitalWrite(33, HIGH);
		digitalWrite(43, LOW);
		break;

	case 30:
		digitalWrite(33, HIGH);
		digitalWrite(44, LOW);
		break;

	case 31:
		digitalWrite(33, HIGH);
		digitalWrite(45, LOW);
		break;

	case 32:
		digitalWrite(34, HIGH);
		digitalWrite(38, LOW);
		break;

	case 33:
		digitalWrite(34, HIGH);
		digitalWrite(39, LOW);
		break;

	case 34:
		digitalWrite(34, HIGH);
		digitalWrite(40, LOW);
		break;

	case 35:
		digitalWrite(34, HIGH);
		digitalWrite(41, LOW);
		break;

	case 36:
		digitalWrite(34, HIGH);
		digitalWrite(42, LOW);
		break;

	case 37:
		digitalWrite(34, HIGH);
		digitalWrite(43, LOW);
		break;

	case 38:
		digitalWrite(34, HIGH);
		digitalWrite(44, LOW);
		break;

	case 39:
		digitalWrite(34, HIGH);
		digitalWrite(45, LOW);
		break;

	case 40:
		digitalWrite(35, HIGH);
		digitalWrite(38, LOW);
		break;

	case 41:
		digitalWrite(35, HIGH);
		digitalWrite(39, LOW);
		break;

	case 42:
		digitalWrite(35, HIGH);
		digitalWrite(40, LOW);
		break;

	case 43:
		digitalWrite(35, HIGH);
		digitalWrite(41, LOW);
		break;

	case 44:
		digitalWrite(35, HIGH);
		digitalWrite(42, LOW);
		break;

	case 45:
		digitalWrite(35, HIGH);
		digitalWrite(43, LOW);
		break;

	case 46:
		digitalWrite(35, HIGH);
		digitalWrite(44, LOW);
		break;

	case 47:
		digitalWrite(35, HIGH);
		digitalWrite(45, LOW);
		break;

	case 48:
		digitalWrite(36, HIGH);
		digitalWrite(38, LOW);
		break;

	case 49:
		digitalWrite(36, HIGH);
		digitalWrite(39, LOW);
		break;

	case 50:
		digitalWrite(36, HIGH);
		digitalWrite(40, LOW);
		break;

	case 51:
		digitalWrite(36, HIGH);
		digitalWrite(41, LOW);
		break;

	case 52:
		digitalWrite(36, HIGH);
		digitalWrite(42, LOW);
		break;

	case 53:
		digitalWrite(36, HIGH);
		digitalWrite(43, LOW);
		break;

	case 54:
		digitalWrite(36, HIGH);
		digitalWrite(44, LOW);
		break;

	case 55:
		digitalWrite(37, HIGH);
		digitalWrite(38, LOW);
		break;

	case 56:
		digitalWrite(37, HIGH);
		digitalWrite(39, LOW);
		break;

	case 57:
		digitalWrite(37, HIGH);
		digitalWrite(40, LOW);
		break;

	case 58:
		digitalWrite(37, HIGH);
		digitalWrite(41, LOW);
		break;

	case 59:
		digitalWrite(37, HIGH);
		digitalWrite(42, LOW);
		break;
	}
}

void MinuteSet()
{
	switch (clock.Minute)
	{
	case 1:
		digitalWrite(46, HIGH);
		digitalWrite(28, LOW);
		break;

	case 2:
		digitalWrite(46, HIGH);
		digitalWrite(27, LOW);
		break;

	case 3:
		digitalWrite(46, HIGH);
		digitalWrite(26, LOW);
		break;

	case 4:
		digitalWrite(46, HIGH);
		digitalWrite(25, LOW);
		break;

	case 5:
		digitalWrite(46, HIGH);
		digitalWrite(24, LOW);
		break;

	case 6:
		digitalWrite(46, HIGH);
		digitalWrite(23, LOW);
		break;

	case 7:
		digitalWrite(46, HIGH);
		digitalWrite(22, LOW);
		break;

	case 8:
		digitalWrite(47, HIGH);
		digitalWrite(29, LOW);
		break;

	case 9:
		digitalWrite(47, HIGH);
		digitalWrite(28, LOW);
		break;

	case 10:
		digitalWrite(47, HIGH);
		digitalWrite(27, LOW);
		break;

	case 11:
		digitalWrite(47, HIGH);
		digitalWrite(26, LOW);
		break;

	case 12:
		digitalWrite(47, HIGH);
		digitalWrite(25, LOW);
		break;

	case 13:
		digitalWrite(47, HIGH);
		digitalWrite(24, LOW);
		break;

	case 14:
		digitalWrite(47, HIGH);
		digitalWrite(23, LOW);
		break;

	case 15:
		digitalWrite(47, HIGH);
		digitalWrite(22, LOW);
		break;

	case 16:
		digitalWrite(48, HIGH);
		digitalWrite(29, LOW);
		break;

	case 17:
		digitalWrite(48, HIGH);
		digitalWrite(28, LOW);
		break;

	case 18:
		digitalWrite(48, HIGH);
		digitalWrite(27, LOW);
		break;

	case 19:
		digitalWrite(48, HIGH);
		digitalWrite(26, LOW);
		break;

	case 20:
		digitalWrite(48, HIGH);
		digitalWrite(25, LOW);
		break;

	case 21:
		digitalWrite(48, HIGH);
		digitalWrite(24, LOW);
		break;

	case 22:
		digitalWrite(48, HIGH);
		digitalWrite(23, LOW);
		break;

	case 23:
		digitalWrite(48, HIGH);
		digitalWrite(22, LOW);
		break;

	case 24:
		digitalWrite(49, HIGH);
		digitalWrite(29, LOW);
		break;

	case 25:
		digitalWrite(49, HIGH);
		digitalWrite(28, LOW);
		break;

	case 26:
		digitalWrite(49, HIGH);
		digitalWrite(27, LOW);
		break;

	case 27:
		digitalWrite(49, HIGH);
		digitalWrite(26, LOW);
		break;

	case 28:
		digitalWrite(49, HIGH);
		digitalWrite(25, LOW);
		break;

	case 29:
		digitalWrite(49, HIGH);
		digitalWrite(24, LOW);
		break;

	case 30:
		digitalWrite(49, HIGH);
		digitalWrite(23, LOW);
		break;

	case 31:
		digitalWrite(49, HIGH);
		digitalWrite(22, LOW);
		break;

	case 32:
		digitalWrite(50, HIGH);
		digitalWrite(29, LOW);
		break;

	case 33:
		digitalWrite(50, HIGH);
		digitalWrite(28, LOW);
		break;

	case 34:
		digitalWrite(50, HIGH);
		digitalWrite(27, LOW);
		break;

	case 35:
		digitalWrite(50, HIGH);
		digitalWrite(26, LOW);
		break;

	case 36:
		digitalWrite(50, HIGH);
		digitalWrite(25, LOW);
		break;

	case 37:
		digitalWrite(50, HIGH);
		digitalWrite(24, LOW);
		break;

	case 38:
		digitalWrite(50, HIGH);
		digitalWrite(23, LOW);
		break;

	case 39:
		digitalWrite(50, HIGH);
		digitalWrite(22, LOW);
		break;

	case 40:
		digitalWrite(51, HIGH);
		digitalWrite(29, LOW);
		break;

	case 41:
		digitalWrite(51, HIGH);
		digitalWrite(28, LOW);
		break;

	case 42:
		digitalWrite(51, HIGH);
		digitalWrite(27, LOW);
		break;

	case 43:
		digitalWrite(51, HIGH);
		digitalWrite(26, LOW);
		break;

	case 44:
		digitalWrite(51, HIGH);
		digitalWrite(25, LOW);
		break;

	case 45:
		digitalWrite(51, HIGH);
		digitalWrite(24, LOW);
		break;

	case 46:
		digitalWrite(51, HIGH);
		digitalWrite(23, LOW);
		break;

	case 47:
		digitalWrite(51, HIGH);
		digitalWrite(22, LOW);
		break;

	case 48:
		digitalWrite(52, HIGH);
		digitalWrite(29, LOW);
		break;

	case 49:
		digitalWrite(52, HIGH);
		digitalWrite(28, LOW);
		break;

	case 50:
		digitalWrite(52, HIGH);
		digitalWrite(27, LOW);
		break;

	case 51:
		digitalWrite(52, HIGH);
		digitalWrite(26, LOW);
		break;

	case 52:
		digitalWrite(52, HIGH);
		digitalWrite(25, LOW);
		break;

	case 53:
		digitalWrite(52, HIGH);
		digitalWrite(24, LOW);
		break;

	case 54:
		digitalWrite(52, HIGH);
		digitalWrite(23, LOW);
		break;

	case 55:
		digitalWrite(53, HIGH);
		digitalWrite(29, LOW);
		break;

	case 56:
		digitalWrite(53, HIGH);
		digitalWrite(28, LOW);
		break;

	case 57:
		digitalWrite(53, HIGH);
		digitalWrite(27, LOW);
		break;

	case 58:
		digitalWrite(53, HIGH);
		digitalWrite(26, LOW);
		break;

	case 59:
		digitalWrite(53, HIGH);
		digitalWrite(25, LOW);
		break;

	case 00:
		digitalWrite(46, HIGH);
		digitalWrite(29, LOW);
		break;
	}
}

void HourSet()
{
	switch (clock.Hour)
	{
	case 1:
		digitalWrite(6, HIGH);
		digitalWrite(19, LOW);
		break;

	case 2:
		digitalWrite(2, HIGH);
		digitalWrite(14, LOW);
		break;

	case 3:
		digitalWrite(18, HIGH);
		digitalWrite(14, LOW);
		break;

	case 4:
		digitalWrite(6, HIGH);
		digitalWrite(14, LOW);
		break;

	case 5:
		digitalWrite(2, HIGH);
		digitalWrite(5, LOW);
		break;

	case 6:
		digitalWrite(18, HIGH);
		digitalWrite(5, LOW);
		break;

	case 7:
		digitalWrite(6, HIGH);
		digitalWrite(5, LOW);
		break;

	case 8:
		digitalWrite(2, HIGH);
		digitalWrite(4, LOW);
		break;

	case 9:
		digitalWrite(18, HIGH);
		digitalWrite(4, LOW);
		break;

	case 10:
		digitalWrite(6, HIGH);
		digitalWrite(4, LOW);
		break;

	case 11:
		digitalWrite(2, HIGH);
		digitalWrite(19, LOW);
		break;

	case 12:
		digitalWrite(17, HIGH);
		digitalWrite(18, HIGH);
		digitalWrite(19, LOW);
		break;

	case 13:
		digitalWrite(6, HIGH);
		digitalWrite(15, HIGH);
		digitalWrite(19, LOW);
		break;

	case 14:
		digitalWrite(2, HIGH);
		digitalWrite(16, HIGH);
		digitalWrite(14, LOW);
		break;

	case 15:
		digitalWrite(17, HIGH);
		digitalWrite(18, HIGH);
		digitalWrite(14, LOW);
		break;

	case 16:
		digitalWrite(6, HIGH);
		digitalWrite(15, HIGH);
		digitalWrite(14, LOW);
		break;

	case 17:
		digitalWrite(16, HIGH);
		digitalWrite(2, HIGH);
		digitalWrite(5, LOW);
		break;

	case 18:
		digitalWrite(17, HIGH);
		digitalWrite(18, HIGH);
		digitalWrite(5, LOW);
		break;

	case 19:
		digitalWrite(6, HIGH);
		digitalWrite(15, HIGH);
		digitalWrite(5, LOW);
		break;

	case 20:
		digitalWrite(16, HIGH);
		digitalWrite(2, HIGH);
		digitalWrite(4, LOW);
		break;

	case 21:
		digitalWrite(17, HIGH);
		digitalWrite(18, HIGH);
		digitalWrite(4, LOW);
		break;

	case 22:
		digitalWrite(6, HIGH);
		digitalWrite(15, HIGH);
		digitalWrite(4, LOW);
		break;

	case 23:
		digitalWrite(2, HIGH);
		digitalWrite(16, HIGH);
		digitalWrite(19, LOW);
		break;

	case 00:
		digitalWrite(17, HIGH);
		digitalWrite(19, LOW);
		break;
	}
}

void LEDClear()
{
	int a = 22;

	while (a <= 29)
	{
		digitalWrite(a, HIGH);
		a++;
	}
	while (a <= 37)
	{
		digitalWrite(a, LOW);
		a++;
	}
	while (a <= 45)
	{
		digitalWrite(a, HIGH);
		a++;
	}

	while (a <= 53)
	{
		digitalWrite(a, LOW);
		a++;
	}
	a = 22;

	while (a <= 29)
	{
		digitalWrite(a, HIGH);
		a++;
	}
	while (a <= 37)
	{
		digitalWrite(a, LOW);
		a++;
	}
	while (a <= 45)
	{
		digitalWrite(a, HIGH);
		a++;
	}

	while (a <= 53)
	{
		digitalWrite(a, LOW);
		a++;
	}
	digitalWrite(15, LOW);
	digitalWrite(16, LOW);
	digitalWrite(17, LOW);
	digitalWrite(18, LOW);
	digitalWrite(2, LOW);
	digitalWrite(6, LOW);

	digitalWrite(4, HIGH);
	digitalWrite(5, HIGH);
	digitalWrite(14, HIGH);
	digitalWrite(19, HIGH);
}

void LCD()
{
	lcd.clear();
	lcd.setCursor(0, 0);
	lcd.noBacklight();
	lcd.print("DATA: ");
	lcd.print(clock.Day);
	lcd.print("/");
	lcd.print(clock.Month);
	lcd.print("/");
	lcd.print(tmYearToCalendar(clock.Year));
}

void setup()
{
	lcd.begin();
	RTC.read(clock);
	LCD();

	pinMode(11, INPUT_PULLUP);
	pinMode(12, INPUT_PULLUP);
	pinMode(13, INPUT_PULLUP);

	int a = 2;
	while (a < 7)
	{
		pinMode(a, OUTPUT);
		a++;
	}

	a = 14;
	while (a < 20)
	{
		pinMode(a, OUTPUT);
		a++;
	}

	a = 22;
	while (a < 54)
	{
		pinMode(a, OUTPUT);
		a++;
	}
	LEDClear();
}

void loop()
{
	RTC.read(clock);
	if (clock.Hour == 0 && clock.Minute == 0 && clock.Second == 1)
		LCD();
	delay(100);
	LEDClear();
	MinuteSet();
	SecondSet();
	HourSet();

	if (digitalRead(12) == LOW)
		setAlarm();

	alarm();
}